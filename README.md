#企业资源整合_客户资源重复利用_客户圈客户群_让 你的客户与客户之间创造更多的价值

 **商业逻辑：** 
1：企业为自己的客户创造了他们之间的互动，让你的客户除了本身享受应该有的服务以外，额外增加了一个增加人脉的机会；

2：扩大了你本身的影响力，同时因为客户带动客户，增强了客户的粘性，提高了后期客户的续费了或者再服务的机会；

3：随着客户的不断扩张，形成一个很好的良性循环。你作为中间信任的基础，将会介绍更多的业务到您这边来做。

4：如果您想，还能抽取部分佣金哦

 **商业逻辑** 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0221/101250_cb856853_4940443.png "企业.png")

以下为摄影行业演示：凸显摄影行业的店铺佣金
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/183910_2c726107_4940443.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/183923_93d83d46_4940443.jpeg "2.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/183957_9da87000_4940443.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184007_314e8a12_4940443.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184016_645d8ab0_4940443.png "5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184026_b28ddf5a_4940443.png "6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184036_ddd86269_4940443.png "7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184058_5cc4c76a_4940443.png "8.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184047_6d6cbe69_4940443.png "9.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184107_3bc977c0_4940443.png "10.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184144_31b93b55_4940443.png "微信图片_20200721184129.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/145323_aa8d7f20_4940443.jpeg "555.jpg")


演示后台：
ktcyl.hnktwl.com/admin
演示账号:admin2
演示密码：admin2
### :tw-1f427: QQ客服
 QQ号:2383177967 
 :fa-fax: 联系电话：15574854597
###  客服二维码
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/184544_98f5627e_4940443.jpeg "kf.jpg")

更新排期文档：
【腾讯文档】内容（可查看）
https://docs.qq.com/sheet/DWkdSQ3REd3FreGF6

更新文档：功能需求征集中（可编辑）
【腾讯文档】平台能够发布商品，未入驻或者已入入驻的上架都可以带货，并且产生带货佣金
https://docs.qq.com/sheet/DWkdqUlJLaURWRWR5

